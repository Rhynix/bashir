const pkg = require('./package')

module.exports = {
  // mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: "The Pediatric Clinic",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      {name: 'author', content: 'John Wanyoike'},

      {property: 'og:title', content: 'Dr Bashir Admani And Associates Pediatric Clinic | Your child\'s health' },
      {property: 'og:type', content: 'Website' },
      {property: 'og:description', content: 'We have provided for the health needs of infants, children, and adolescents since 2008.' },
      {property: 'og:site_name', content: 'The Pediatric Clinic' },
      {property: 'og:url', content: 'http://www.pedbashir.com' },
      {property: 'og:image', content: 'http://www.pedbashir.com/images/logo.png' },
      {property: 'article:published_time', content: '2019-03-19T13:50:44+03:00' },
      {property: 'article:modified_time', content: '2019-03-19T13:50:44+03:00' },

      {property: 'og:locale', content: 'en_KE' },
      {property: 'og:locale:alternate', content: 'en_KE' },

      {itemprop: 'name', content: 'Dr Bashir Admani And Associates Pediatric Clinic' },
      {itemprop: 'description', content: 'We have provided for the health needs of infants, children, and adolescents since 2008. We are dedicated to providing the highest quality health care for our patients in accordance with international standards.' },
      {itemprop: 'image', content: 'http://www.pedbashir.com/images/logo.png' },

      {hid: 'keyword', name: 'keyword', content: 'Bashir Admani, Dr Bashir Admani, Pediatric Clinic, Pediatric, Clinic, Pediatrician, Child Doctor, Sick Child, Clinic, Doctors in Nairobi, Doctors in Parklands, Paediatrician, Child Clinic, Kid Clinic, Nephrology, Kidney, Kidney Doctor, Children Kidney, Renal, Kidney Renal, Children Renal' },
      {hid: 'description', name: 'description', content: 'The Pediatric Clinic' },
      
      {name: 'msapplication-TileColor', content: '#ffffff' },
      {name: 'theme-color', content: '#ffffff' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700'},
      {rel: 'stylesheet', href: 'https://unpkg.com/element-ui/lib/theme-chalk/index.css'},
      {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css'},
    ],
    script: [
      { src: '//code.jquery.com/jquery-3.3.1.min.js' },
      { src: 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' }
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#FFFFFF' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/font-awesome/css/fontawesome-all.min.css',
    '~/assets/animate.css/animate.min.css',
    '~/assets/hs-megamenu/src/hs.megamenu.css',
    // '~/assets/vendor/fancybox/jquery.fancybox.css',
    '~/assets/slick-carousel/slick/slick.css',
    '~/assets/css/manifest.webmanifest.json',
    '~/assets/css/theme.css',
    'fullcalendar/dist/fullcalendar.min.css'
  ],
  script: [
    {src: '/vendor/jquery/dist/jquery.min.js',body: true},
    {src: '/vendor/jquery/dist/jquery.min.js',body: true},
    {src: '/vendor/jquery-migrate/dist/jquery-migrate.min.js',body: true},
    {src: '/vendor/popper.js/dist/umd/popper.min.js',body: true},
    {src: '/vendor/bootstrap/bootstrap.min.js',body: true},
    
    {src: '/vendor/hs-megamenu/src/hs.megamenu.js',body: true},
    {src: '/vendor/svg-injector/dist/svg-injector.min.js',body: true},
    {src: '/vendor/jquery-validation/dist/jquery.validate.min.js',body: true},
    {src: '/vendor/slick-carousel/slick/slick.js',body: true},

    {src: '/js/index.js'},
    {src: '/js/sw.js'},

    {src: '/js/hs.core.js',body: true},
    {src: '/js/components/hs.header.js',body: true},
    {src: '/js/components/hs.focus-state.js',body: true},
    {src: '/js/components/hs.validation.js',body: true},
    {src: '/js/components/hs.slick-carousel.js',body: true},
    {src: '/js/components/hs.svg-injector.js',body: true},
    {src: '/js/components/hs.go-to.js',body: true},
    // {src: '/vendor/fancybox/jquery.fancybox.min.js',body: true},
    {src: '/js/components/hs.fancybox.js',body: true},
    {src: '/vendor/typed.js/lib/typed.min.js',body: true},
    {src: '/js/scripts.js',body: true},
  ],

  /*
  ** Plugins to load before mounting the App
  */
 plugins: [
  './plugins/scrollto.js',
  './plugins/validation.js',
  './plugins/axios.js',
  { src: "~/plugins/vue2-google-maps", ssr: true },
  { src: '~/plugins/vue-fullcalendar', ssr: false },
  { src: '~/plugins/jquery', ssr: false }
],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/onesignal',
    '@nuxtjs/pwa',
    '@nuxtjs/axios',
    'vue-sweetalert2/nuxt',
    // '@nuxtjs/auth',
    'nuxt-element-ui',
    // 'vue-sweetalert2/nuxt'
  ],
  oneSignal: {
    cdn: true,
    OneSignalSDK: 'https://cdn.onesignal.com/sdks/OneSignalSDK.js',
    init: {
      appId: '7274f452-b838-4ff2-a556-037a6e0b5694',
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
          disable: false
      }
    }
  },
  elementUI: {
    components: ['Button', 'DatePicker', 'TimePicker', 'TimeSelect'],
    locale: 'en',
  },
  /*
  ** Axios module configuration
  */
  axios: {
    baseURL: 'https://node.fleetwise.co/'
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vue2-google-maps($|\/)/],
    vendor:[
      '/vendor/jquery/dist/jquery.min.js',
      '/vendor/typed.js/lib/typed.min.js',
      '/js/scripts.js',
    ],
    extractCSS: true,
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      //add for vue2-google-maps
      // if (!ctx.isClient) {
      //   // This instructs Webpack to include `vue2-google-maps`'s Vue files
      //   // for server-side rendering
      //   config.externals.splice(0, 0, function (context, request, callback) {
      //     if (/^vue2-google-maps($|\/)/.test(request)) {
      //       callback(null, false)
      //     } else {
      //       callback()
      //     }
      //   })
      // }
    }
  }
}
