import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCwjL8X0GqfL4ZoHu8XJ3V6QiQtqm8WnJo',
    libraries: 'places', 
  },
})