export const state = () => ({
  services: [{
      "id": 1,
      "icon": "baby.svg",
      "servicedone": "Our services",
      "doctor": "Dr. Bashir Admani.",
      "deacription": {
        "desc1": [{
            "id": 1,
            "name": "Neonatal Care",
            "class": "success",
            "desc": "We provide parental discussion...",
            "lt": "N",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "We provide parental discussion and counselling for parents to be.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "We also provide neonatal care as we would receive the baby, do full examination and manage the baby until discharge and following thereafter",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Your baby's muscle skills have developed enough to eat solid foods safely. Some of the things that show you that your baby is ready to try solid foods include:",
                "list": [{
                    "id": 1,
                    "name": "Being able to sit with support."
                  },
                  {
                    "id": 2,
                    "name": "Good head and neck control."
                  },
                  {
                    "id": 3,
                    "name": "Placing hands and toys in the mouth."
                  },
                  {
                    "id": 4,
                    "name": "Leaning forward when interested in food."
                  },
                  {
                    "id": 5,
                    "name": "Leaning back and turning the head when not interested in food."
                  },
                ]
              }
            ]
          },
          {
            "id": 2,
            "name": "Developmental Care",
            "class": "warning",
            "desc": "Start feeding your infant...",
            "lt": "D",
            "files":[
                {"file": "car_seat_policy.pdf", "desc": "Developmental Check Timetable"},
            ],
            "details": [{
                "title": "WHEN TO START FEEDING YOUR BABY SOLID FOOD",
                "paragraph": "Start feeding your infant solid food when your baby's caregiver recommends it. Most experts suggest waiting until",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Your baby is around 6 months old.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Your baby's muscle skills have developed enough to eat solid foods safely. Some of the things that show you that your baby is ready to try solid foods include:",
                "list": [{
                    "id": 1,
                    "name": "Being able to sit with support."
                  },
                  {
                    "id": 2,
                    "name": "Good head and neck control."
                  },
                  {
                    "id": 3,
                    "name": "Placing hands and toys in the mouth."
                  },
                  {
                    "id": 4,
                    "name": "Leaning forward when interested in food."
                  },
                  {
                    "id": 5,
                    "name": "Leaning back and turning the head when not interested in food."
                  },
                ]
              },
              {
                "title": "HOW TO START FEEDING YOUR BABY SOLID FOOD",
                "paragraph": "Choose a time when you are both relaxed. Right after or in the middle of a normal feeding is a good time to introduce solid food. Do not try this when your baby is too hungry. At first, some of the food may come back out of the mouth. Babies often do not know how to swallow solid food at first. Your child may need practice to eat solid foods well.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Start with rice or a single-grain infant cereal with added vitamins and minerals. Start with 1 or 2 teaspoons of dry cereal. Mix this with enough formula or breast milk to make a thin liquid. Begin with just a small amount on the tip of the spoon. Over time you can make the cereal thicker and offer more at each feeding. Add a second solid feeding as needed. You can also give your baby small amounts of pureed fruit, vegetables, and meat.",
                "list": []
              },
              {
                "title": "Some important points to remember",
                "paragraph": "Start with rice or a single-grain infant cereal with added vitamins and minerals. Start with 1 or 2 teaspoons of dry cereal. Mix this with enough formula or breast milk to make a thin liquid. Begin with just a small amount on the tip of the spoon. Over time you can make the cereal thicker and offer more at each feeding. Add a second solid feeding as needed. You can also give your baby small amounts of pureed fruit, vegetables, and meat.",
                "list": [{
                    "id": 1,
                    "name": "Solid foods should not replace breastfeeding or bottle-feeding"
                  },
                  {
                    "id": 2,
                    "name": "First solid foods should always be pureed"
                  },
                  {
                    "id": 3,
                    "name": "Additives like sugar or salt are not needed"
                  },
                  {
                    "id": 4,
                    "name": "Always use single-ingredient foods so you will know what causes a reaction. Take at least 3 or 4 days before introducing each new food. By doing this, you will know if your baby has problems with one of the foods. Problems may include diarrhea, vomiting, constipation, fussiness, or rash"
                  },
                  {
                    "id": 5,
                    "name": "Do not add cereal or solid foods to your baby's bottle"
                  },
                  {
                    "id": 6,
                    "name": "Always feed solid foods with your baby's head upright"
                  },
                  {
                    "id": 7,
                    "name": "Always make sure foods are not too hot before giving them to your baby."
                  },
                  {
                    "id": 8,
                    "name": "Do not force to feed your baby. Your baby will let you when he or she is full. If your baby leans back in the chair, turns his or her head away from food, starts playing with the spoon, or refuses to open up his or her mouth for the next bite, he or she has probably had enough"
                  },
                  {
                    "id": 9,
                    "name": "Many foods will change the color and consistency of your infants stool. Some foods may make your baby's stool hard. If some foods cause constipation, such as rice cereal, bananas, or applesauce, switch to other fruits or vegetables or oatmeal or barley cereal"
                  },
                  {
                    "id": 10,
                    "name": "Finger foods can be introduced around 9 months of age"
                  },
                ]
              },
              {
                "title": "Food to avoid",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Honey in babies younger than 1 year . It can cause botulism."
                  },
                  {
                    "id": 2,
                    "name": "Cow's milk under in babies younger than 1 year."
                  },
                  {
                    "id": 3,
                    "name": "Foods that have already caused a bad reaction."
                  },
                  {
                    "id": 4,
                    "name": "Choking foods, such as grapes, hot dogs, popcorn, raw carrots and other vegetables, nuts, and candies."
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Go very slow with foods that are common causes of allergic reaction. It is not clear if delaying the introduction of allergenic foods will change your child's likelihood of having a food allergy. If you start these foods, begin with just a taste. If there are no reactions after a few days, try it again in gradually larger amounts. Examples of allergenic foods include:",
                "list": []
              },
              {
                "title": "",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Shellfish"
                  },
                  {
                    "id": 2,
                    "name": "Eggs and egg products, such as custard"
                  },
                  {
                    "id": 3,
                    "name": "Nut products"
                  },
                  {
                    "id": 4,
                    "name": "Cow's milk and milk products"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Do not force to feed your baby. Your baby will let you when he or she is full. If your baby leans back in the chair, turns his or her head away from food, starts playing with the spoon, or refuses to open up his or her mouth for the next bite, he or she has probably had enough.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Many foods will change the color and consistency of your infants stool. Some foods may make your baby's stool hard. If some foods cause constipation, such as rice cereal, bananas, or applesauce, switch to other fruits or vegetables or oatmeal or barley cereal",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Finger foods can be introduced around 9 months of age",
                "list": []
              },
            ]
          },
          {
            "id": 3,
            "name": "Immunizations",
            "class": "danger",
            "desc": "We provide immunisations for infants and school going children. The following is a list of the vaccines offered at the clinics",
            "lt": "I",
            "files":[],
            "details": [{
                "title": "Birth",
                "paragraph": "",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Hep B: Hepatitis B vaccine (HBV); recommended to give the first dose at birth, but may be given at any age for those not previously immunized.",
                "list": [{
                    "id": 1,
                    "name": "OPV: oral polio vaccine"
                  },
                  {
                    "id": 2,
                    "name": "BCG: for Tuberculosis"
                  },
                ]
              },
              {
                "title": "2 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "DTaP: Diphtheria, tetanus, and acellular pertussis vaccine"
                  },
                  {
                    "id": 2,
                    "name": "Hib: Haemophilus influenzae type b vaccine (INFANRIX HEXA #1)"
                  },
                  {
                    "id": 3,
                    "name": "IPV: Inactivated poliovirus vaccine."
                  },
                  {
                    "id": 4,
                    "name": "Hep B"
                  },
                  {
                    "id": 5,
                    "name": "PCV: Pneumococcal conjugate vaccine   (PREVENAR)"
                  },
                  {
                    "id": 6,
                    "name": "Rota: Rotavirus vaccine   (ROTATEQ)"
                  },
                ]
              },
              {
                "title": "3 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "DTaP"
                  },
                  {
                    "id": 2,
                    "name": "Hib PENTAXIM"
                  },
                  {
                    "id": 3,
                    "name": "IPV"
                  },
                  {
                    "id": 4,
                    "name": "PCV (PREVENAR)"
                  },
                  {
                    "id": 5,
                    "name": "Rota (ROTATEQ)"
                  },
                ]
              },
              {
                "title": "4 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "DTaP"
                  },
                  {
                    "id": 2,
                    "name": "Hib PENTAXIM"
                  },
                  {
                    "id": 3,
                    "name": "IPV"
                  },
                  {
                    "id": 4,
                    "name": "PCV (PREVENAR)"
                  },
                  {
                    "id": 5,
                    "name": "Rota (ROTATEQ)"
                  },
                ]
              },
              {
                "title": "6 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Hep B"
                  },
                  {
                    "id": 2,
                    "name": "+/- Flu Vaccine"
                  },
                ]
              },
              {
                "title": "10 Months",
                "paragraph": "",
                "list": [{
                  "id": 1,
                  "name": "Menactra"
                }, ]
              },
              {
                "title": "12 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "MMR: Measles, mumps, and rubella (German measles) vaccine"
                  },
                  {
                    "id": 2,
                    "name": "Hep A #1"
                  },
                  {
                    "id": 3,
                    "name": "Varicella (chickenpox) vaccine"
                  },
                ]
              },
              {
                "title": "18 months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "DTaP"
                  },
                  {
                    "id": 2,
                    "name": "IPV PENTAXIM"
                  },
                  {
                    "id": 3,
                    "name": "HIB"
                  },
                  {
                    "id": 4,
                    "name": "PCV #4"
                  },
                  {
                    "id": 5,
                    "name": "Hep A #2"
                  },
                ]
              },
              {
                "title": "2Years",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Meningococcal ACYW"
                  },
                  {
                    "id": 2,
                    "name": "Typhoid"
                  },
                ]
              },
              {
                "title": "4–6 years",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "DTaP"
                  },
                  {
                    "id": 2,
                    "name": "MMR"
                  },
                  {
                    "id": 3,
                    "name": "IPV"
                  },
                ]
              },
              {
                "title": "11–12 years",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "HPV: Human papillomavirus (HPV) vaccine, given as 3 shots over 6 months. It’s recommended for girls ages 11 or 12, and also recommended for girls ages 13 to 18 if they have not yet been vaccinated. The vaccine also may be given to boys ages 9 to 18"
                  },
                  {
                    "id": 2,
                    "name": "Tdap: Tetanus, diphtheria, and pertussis booster"
                  },
                ]
              },

            ]
          },
          {
            "id": 4,
            "name": "Hearing Test",
            "class": "primary",
            "desc": "We routinely do hearing test",
            "lt": "H",
            "files":[],
            "details": [{
              "title": "",
              "paragraph": "We routinely do hearing test (DPOAE) for babies on the first visit after birth and provide a certificate for the same. We repeat hearing test if needed at one year.",
              "list": []
            }, ]
          },
          
        ],
        "desc2": [
            {
              "id": 0,
              "name": "Urgent Medical Visits",
              "class": "info",
              "desc": "In case of urgent medical v...",
              "lt": "U",
              "files":[],
              "details": [{
                "title": "",
                "paragraph": "In case of urgent medical visits, call 0733407998 or 0721967818 and the child will be seen at the earliest possible time.",
                "list": []
              }, ]
            },{
            "id": 1,
            "name": "Emergency on call services",
            "class": "primary",
            "desc": "We provide emergency care services for our patients on the following Numbers: Aga Khan clinic-0721967818, The paediatric clinic Karen -0721216289 and The paediatric clinic Lavington - 0722726884",
            "lt": "E",
            "files":[],
            "details": [{
              "title": "Contacts",
              "paragraph": "In case of emergency, call or email us on the numbers listed below and the doctor on call will answer and advice accordingly or call the ambulance number. The emergency lines are open 24 hours a day, every day.",
              "list": [{
                  "id": 1,
                  "name": "0721967818 (The paediatric clinic Aga Khan)"
                },
                {
                  "id": 1,
                  "name": "0721216289 (The paediatric clinic Karen)"
                },
                {
                  "id": 1,
                  "name": "0722726884 (The paediatric clinic Lavington)"
                },
                {
                  "id": 2,
                  "name": "drbashiradmani@gmail.com"
                },
                {
                  "id": 3,
                  "name": "booking@pediatriclinickaren.com"
                },
              ]
            }, ]
          },
          {
            "id": 3,
            "name": "Specialised Nephrology Care",
            "class": "success",
            "desc": "We provide consultation and mana...",
            "lt": "S",
            "files":[],
            "details": [{
              "title": "",
              "paragraph": "We provide consultation and management of pediatric renal disease",
              "list": [{
                  "id": 1,
                  "name": "Uti"
                },
                {
                  "id": 2,
                  "name": "Hypertension"
                },
                {
                  "id": 3,
                  "name": "Obstructive uropathy"
                },
                {
                  "id": 4,
                  "name": "Acute kidney injury"
                },
                {
                  "id": 5,
                  "name": "Chronic kidney disease"
                },
                {
                  "id": 6,
                  "name": "Dialysis"
                },
                {
                  "id": 7,
                  "name": "Kidney transplant care"
                },
                {
                  "id": 8,
                  "name": "Urolithiasis"
                },
                {
                  "id": 9,
                  "name": "Nephrotic syndrome"
                },
                {
                  "id": 10,
                  "name": "Nephritis and others"
                },
              ]
            }, ]
          },
          {
            "id": 4,
            "name": "The Pediatric Clinic Karen",
            "class": "warning",
            "desc": "The Karen clinic is already operational ...",
            "lt": "T",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "The Karen clinic is already operational it is located at the HUB Karen on first floor.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "For more contact:",
                "list": [{
                    "id": 1,
                    "name": "0718679933"
                  },
                  {
                    "id": 2,
                    "name": "0732679933"
                  },
                  {
                    "id": 3,
                    "name": "drbashiradmani@gmail.com"
                  },
                  {
                    "id": 4,
                    "name": "booking@pediatriclinickaren.com"
                  },
                ]
              },
            ]
          },
        ],
      },
    },
    {
      "id": 2,
      "icon": "skyroskope.svg",
      "servicedone": "Development Checkups",
      "doctor": "Dr. Farida Essajee.",
      "deacription": {
        "desc1": [{
            "id": 1,
            "name": "At 1 Month & 2 Months",
            "class": "danger",
            "desc": "TAt 1 Month...",
            "lt": "1",
            "files":[
                {"file": "ASQ-3-2-Mo-Set-B-download-3-1-to-2-months.pdf", "desc": "1 to 2 Months"},
            ],
            "details": [{
                "title": "At 1 Month",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Momentarily holds up head when supported"
                  },
                  {
                    "id": 2,
                    "name": "Avoids slightly annoying sensations, such as cloth put on face"
                  },
                  {
                    "id": 3,
                    "name": "Some 'gurgling' sounds"
                  },
                  {
                    "id": 4,
                    "name": "Raises head slightly off bed or floor when lying on stomach"
                  },
                  {
                    "id": 5,
                    "name": "Briefly watches and follows object with eyes"
                  },
                ]
              },
              {
                "title": "At 2 Months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Vocalizes"
                  },
                  {
                    "id": 2,
                    "name": "Mimics or responds to smiling person with occasional smile"
                  },
                  {
                    "id": 3,
                    "name": "Follows moving person with eyes"
                  },
                  {
                    "id": 4,
                    "name": "Holds feet erect, bobbing, when supported while sitting"
                  },
                ]
              },
            ]
          },
          {
            "id": 2,
            "name": "At 3 & 4 Months",
            "class": "warning",
            "desc": "At 3 Months...",
            "lt": "3",
            "files":[
                {"file": "asq-4-mo-download-4-at-3-4-months.pdf", "desc": "At 3 & 4 Months"},
            ],
            "details": [{
                "title": "At 3 Months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Lifts chest and head when lying on stomach"
                  },
                  {
                    "id": 2,
                    "name": "Recognizes breast or bottle"
                  },
                  {
                    "id": 3,
                    "name": "Spirited body movement"
                  },
                  {
                    "id": 4,
                    "name": "Chuckles, coos"
                  },
                  {
                    "id": 5,
                    "name": "Improved head control"
                  },
                ]
              },
              {
                "title": "At 4 Months",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Rolls from side to side"
                  },
                  {
                    "id": 2,
                    "name": "Follows moving object when held sitting"
                  },
                  {
                    "id": 3,
                    "name": "Good head control"
                  },
                  {
                    "id": 4,
                    "name": "Laughs"
                  },
                  {
                    "id": 5,
                    "name": "May begin to reach"
                  },
                  {
                    "id": 6,
                    "name": "Enjoys play"
                  },
                  {
                    "id": 7,
                    "name": "Grasps object, such as rattle, held near hand"
                  },
                ]
              },
            ]
          },
          {
            "id": 3,
            "name": "At 6 Months",
            "class": "primary",
            "desc": "At 6 Months...",
            "lt": "6",
            "files":[
                {"file": "ASQ-6-mo-download=5-at-6-months.pdf", "desc": "At 6 Months"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Rolls from back to stomach"
                },
                {
                  "id": 2,
                  "name": "Moves object from hand to hand, hand to mouth"
                },
                {
                  "id": 3,
                  "name": "Sits with little support"
                },
                {
                  "id": 4,
                  "name": "Babbles more than two sounds"
                },
              ]
            }, ]
          },
          {
            "id": 4,
            "name": "At 9 Months",
            "class": "success",
            "desc": "At 6 Months...",
            "lt": "9",
            "files":[
                {"file": "asq-9-download=6-at-9-months.pdf", "desc": "At 9 Months"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Sits by self"
                },
                {
                  "id": 2,
                  "name": "Unwraps block"
                },
                {
                  "id": 3,
                  "name": "Changes position without falling"
                },
                {
                  "id": 4,
                  "name": "Says 'mama,' 'baba' or 'dada'"
                },
                {
                  "id": 5,
                  "name": "Plays with two objects at same time"
                },
              ]
            }, ]
          },
          {
            "id": 5,
            "name": "At 12 Months (1 Year)",
            "desc": "At 12 Months...",
            "class": "info",
            "lt": "12",
            "files":[
                {"file": "ASQ-12-download=7-at-12-months.pdf", "desc": "At 12 Months (1 Year)"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Pulls self-up, stands"
                },
                {
                  "id": 2,
                  "name": "Gives toy on request"
                },
                {
                  "id": 3,
                  "name": "Steps with support"
                },
                {
                  "id": 4,
                  "name": "Displays affection"
                },
                {
                  "id": 5,
                  "name": "Picks up things with thumb and one finger"
                },
                {
                  "id": 7,
                  "name": "Follows simple directions with gesture"
                },
                {
                  "id": 8,
                  "name": "Stacks two blocks"
                },
                {
                  "id": 9,
                  "name": "May say two or more words"
                },
              ]
            }, ]
          },
          {
            "id": 6,
            "name": "At 15 Months",
            "class": "secondary",
            "desc": "At 15 Months...",
            "lt": "15",
            "files":[],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Walks without support"
                },
                {
                  "id": 2,
                  "name": "Says four or five words"
                },
                {
                  "id": 3,
                  "name": "Stacks three blocks"
                },
                {
                  "id": 4,
                  "name": "Vocalizes, voice up and down, sounds like conversation."
                },
                {
                  "id": 5,
                  "name": "Some feeding of self"
                },
                {
                  "id": 7,
                  "name": "Uses gestures to communicate"
                },
              ]
            }, ]
          },
        ],
        "desc2": [{
            "id": 7,
            "name": "At 18 Months",
            "class": "info",
            "desc": "At 18 Months...",
            "lt": "18",
            "files":[
                {"file": "ASQ-18-Months-download=9-18-months.pdf", "desc": "At 18 Months"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Walks, runs a little."
                },
                {
                  "id": 2,
                  "name": "Marks with crayon on paper."
                },
                {
                  "id": 3,
                  "name": "Climbs up or down one stair"
                },
                {
                  "id": 4,
                  "name": "Partially feeds self"
                },
                {
                  "id": 5,
                  "name": "Enjoys pull toys"
                },
                {
                  "id": 7,
                  "name": "Says five to ten words"
                },
                {
                  "id": 8,
                  "name": "Likes being read to"
                },
              ]
            }, ]
          },
          {
            "id": 8,
            "name": "At 24 Months (2 Years)",
            "class": "danger",
            "desc": "At 18 Months...",
            "lt": "24",
            "files":[
                {"file": "ASQ-23-24Months-download=10-asq-23-24months.pdf", "desc": "ASQ 23-24Months"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Kicks large ball."
                },
                {
                  "id": 2,
                  "name": "Recognizes familiar picture, knows if upside down."
                },
                {
                  "id": 3,
                  "name": "Turns pages, two or three at a time."
                },
                {
                  "id": 4,
                  "name": "Asks for items by name."
                },
                {
                  "id": 5,
                  "name": "Imitates housework."
                },
                {
                  "id": 7,
                  "name": "Uses two or three words together, such as 'Want blanket'."
                },
              ]
            }, ]
          },
          {
            "id": 9,
            "name": "At 36 Months (3 Years)",
            "class": "warning",
            "desc": "At 36 Months...",
            "lt": "36",
            "files":[
                {"file": "asq-36-months-3years-download=11-asq-36-months-3years.pdf", "desc": "ASQ 36 MONTHS (3years)"},
            ],
            "details": [{
              "title": "",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Briefly stands on one foot"
                },
                {
                  "id": 2,
                  "name": "Feeds self."
                },
                {
                  "id": 3,
                  "name": "Walks up stairs."
                },
                {
                  "id": 4,
                  "name": "Opens door."
                },
                {
                  "id": 5,
                  "name": "Rides tricycle"
                },
                {
                  "id": 7,
                  "name": "Verbalizes toilet needs"
                },
                {
                  "id": 8,
                  "name": "Speaks in sentences"
                },
              ]
            }, ]
          },
          {
            "id": 10,
            "name": "ADHD Questionnaire",
            "class": "primary",
            "desc": "Questionnaire...",
            "lt": "A",
            "files":[
                {"file": "ADHD-QUESTIONNAIRE-download=12-adhd-questionnaire.pdf", "desc": "ADHD QUESTIONNAIRE"},
            ],
            "details": []
          },
          {
            "id": 11,
            "name": "Autism Assessment Checklist",
            "class": "success",
            "desc": "Autism Assessment Checklist...",
            "lt": "A",
            "files":[],
            "details": []
          },
          {
            "id": 12,
            "name": "MCHAT Questionnaire",
            "class": "danger",
            "desc": "Questionnaire...",
            "lt": "M",
            "files":[
                {"file": "MCHAT-QUESTIONNAIRE-download=13-mchat-questionnaire.pdf", "desc": "MCHAT QUESTIONNAIRE"},
            ],
            "details": []
          },
        ],
      },
    },
    {
      "id": 3,
      "icon": "syrange.svg",
      "servicedone": "Vaccination",
      "doctor": "Dr. Anne-Marie.",
      "deacription": {
        "desc1": [{
          "id": 1,
          "name": "Vaccination schedule",
          "class": "primary",
          "desc": "Hep B: Hepatitis B vaccine (HBV)...",
          "lt": "V",
          "files":[],
          "details": [{
              "title": "Birth",
              "paragraph": "Hep B: Hepatitis B vaccine (HBV); recommended to give the first dose at birth, but may be given at any age for those not previously immunized.",
              "list": [{
                  "id": 1,
                  "name": "OPV: oral polio vaccine"
                },
                {
                  "id": 2,
                  "name": "BCG: for Tuberculosis"
                },
              ]
            },
            {
              "title": "2 months",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "DTaP: Diphtheria, tetanus, and acellular pertussis vaccine"
                },
                {
                  "id": 2,
                  "name": "Hib: Haemophilus influenzae type b vaccine (INFANRIX HEXA #1)"
                },
                {
                  "id": 3,
                  "name": "IPV: Inactivated poliovirus vaccine"
                },
                {
                  "id": 4,
                  "name": "Hep B"
                },
                {
                  "id": 5,
                  "name": "PCV: Pneumococcal conjugate vaccine (PREVENAR)"
                },
                {
                  "id": 6,
                  "name": "Rota: Rotavirus vaccine (ROTATEQ)"
                },
              ]
            },
            {
              "title": "3 months",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "DTaP"
                },
                {
                  "id": 2,
                  "name": "Hib PENTAXIM"
                },
                {
                  "id": 3,
                  "name": "IPV"
                },
                {
                  "id": 4,
                  "name": "PCV (PREVENAR)"
                },
                {
                  "id": 5,
                  "name": "Rota (ROTATEQ)"
                },
              ]
            },
            {
              "title": "4 months",
              "paragraph": "Hep B: Hepatitis B vaccine (HBV); recommended to give the first dose at birth, but may be given at any age for those not previously immunized.",
              "list": [{
                  "id": 1,
                  "name": "DTaP"
                },
                {
                  "id": 2,
                  "name": "Hib PENTAXIM"
                },
                {
                  "id": 3,
                  "name": "IPV"
                },
                {
                  "id": 4,
                  "name": "PCV (PREVENAR)"
                },
                {
                  "id": 5,
                  "name": "Rota (ROTATEQ)"
                },
              ]
            },
            {
              "title": "6 months",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Hep B"
                },
                {
                  "id": 2,
                  "name": "+/- Flu Vaccine"
                },
              ]
            },
            {
              "title": "10 Months",
              "paragraph": "",
              "list": [{
                "id": 1,
                "name": "Menactra"
              }, ]
            },
            {
              "title": "12 months",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "MMR: Measles, mumps, and rubella (German measles) vaccine"
                },
                {
                  "id": 2,
                  "name": "Hep A #1"
                },
                {
                  "id": 3,
                  "name": "Varicella (chickenpox) vaccine"
                },
              ]
            },
            {
              "title": "18 months",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "DTaP"
                },
                {
                  "id": 2,
                  "name": "IPV PENTAXIM"
                },
                {
                  "id": 3,
                  "name": "HIB"
                },
                {
                  "id": 4,
                  "name": "PCV #4"
                },
                {
                  "id": 5,
                  "name": "Hep A #2"
                },
              ]
            },
            {
              "title": "2 Years",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Meningococcal ACYW"
                },
                {
                  "id": 2,
                  "name": "Typhoid"
                },
              ]
            },
            {
              "title": "4–6 years",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "DTaP"
                },
                {
                  "id": 2,
                  "name": "MMR"
                },
                {
                  "id": 3,
                  "name": "IPV"
                },
              ]
            },
            {
              "title": "11–12 years",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "HPV: Human papillomavirus (HPV) vaccine, given as 3 shots over 6 months. It’s recommended for girls ages 11 or 12, and also recommended for girls ages 13 to 18 if they have not yet been vaccinated. The vaccine also may be given to boys ages 9 to 18"
                },
                {
                  "id": 2,
                  "name": "Tdap: Tetanus, diphtheria, and pertussis booster"
                },
              ]
            },
          ]
        }, ],
      }
    },
    {
      "id": 4,
      "icon": "clipboard.svg",
      "servicedone": "Health topics",
      "doctor": "Dr. Bashir Admani.",
      "deacription": {
        "desc1": [{
            "id": 1,
            "name": "When to Start Feeding Your Baby Solid Food",
            "desc": "Hep B: Hepatitis B vaccine (HBV)...",
            "class": "secondary",
            "lt": "W",
            "files":[],
            "details": [{
                "title": "WHEN TO START FEEDING YOUR BABY SOLID FOOD",
                "paragraph": "Start feeding your infant solid food when your baby's caregiver recommends it. Most experts suggest waiting until:",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Your baby is around 6 months old.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Your baby's muscle skills have developed enough to eat solid foods safely. Some of the things that show you that your baby is ready to try solid foods include:",
                "list": [{
                    "id": 1,
                    "name": "Being able to sit with support."
                  },
                  {
                    "id": 2,
                    "name": "Good head and neck control."
                  },
                  {
                    "id": 3,
                    "name": "Placing hands and toys in the mouth."
                  },
                  {
                    "id": 4,
                    "name": "Leaning forward when interested in food."
                  },
                  {
                    "id": 5,
                    "name": "Leaning back and turning the head when not interested in food."
                  },
                ]
              },
              {
                "title": "HOW TO START FEEDING YOUR BABY SOLID FOOD",
                "paragraph": "Choose a time when you are both relaxed. Right after or in the middle of a normal feeding is a good time to introduce solid food. Do not try this when your baby is too hungry. At first, some of the food may come back out of the mouth. Babies often do not know how to swallow solid food at first. Your child may need practice to eat solid foods well.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Start with rice or a single-grain infant cereal with added vitamins and minerals. Start with 1 or 2 teaspoons of dry cereal. Mix this with enough formula or breast milk to make a thin liquid. Begin with just a small amount on the tip of the spoon. Over time you can make the cereal thicker and offer more at each feeding. Add a second solid feeding as needed. You can also give your baby small amounts of pureed fruit, vegetables, and meat.",
                "list": []
              },
              {
                "title": "Some important points to remember:",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Solid foods should not replace breastfeeding or bottle-feeding."
                  },
                  {
                    "id": 2,
                    "name": "First solid foods should always be pureed."
                  },
                  {
                    "id": 3,
                    "name": "Additives like sugar or salt are not needed."
                  },
                  {
                    "id": 4,
                    "name": "Always use single-ingredient foods so you will know what causes a reaction. Take at least 3 or 4 days before introducing each new food. By doing this, you will know if your baby has problems with one of the foods. Problems may include diarrhea, vomiting, constipation, fussiness, or rash."
                  },
                  {
                    "id": 5,
                    "name": "Do not add cereal or solid foods to your baby's bottle."
                  },
                  {
                    "id": 6,
                    "name": "Always feed solid foods with your baby's head upright."
                  },
                  {
                    "id": 7,
                    "name": "Always make sure foods are not too hot before giving them to your baby."
                  },
                  {
                    "id": 8,
                    "name": "Do not force to feed your baby. Your baby will let you when he or she is full. If your baby leans back in the chair, turns his or her head away from food, starts playing with the spoon, or refuses to open up his or her mouth for the next bite, he or she has probably had enough."
                  },
                  {
                    "id": 9,
                    "name": "Many foods will change the color and consistency of your infants stool. Some foods may make your baby's stool hard. If some foods cause constipation, such as rice cereal, bananas, or applesauce, switch to other fruits or vegetables or oatmeal or barley cereal."
                  },
                  {
                    "id": 10,
                    "name": "Finger foods can be introduced around 9 months of age."
                  },
                ]
              },
              {
                "title": "FOODS TO AVOID",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Honey in babies younger than 1 year . It can cause botulism."
                  },
                  {
                    "id": 2,
                    "name": "Cow's milk under in babies younger than 1 year."
                  },
                  {
                    "id": 3,
                    "name": "Foods that have already caused a bad reaction."
                  },
                  {
                    "id": 4,
                    "name": "Choking foods, such as grapes, hot dogs, popcorn, raw carrots and other vegetables, nuts, and candies."
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Go very slow with foods that are common causes of allergic reaction. It is not clear if delaying the introduction of allergenic foods will change your child's likelihood of having a food allergy. If you start these foods, begin with just a taste. If there are no reactions after a few days, try it again in gradually larger amounts. Examples of allergenic foods include:",
                "list": [{
                    "id": 1,
                    "name": "Shellfish"
                  },
                  {
                    "id": 2,
                    "name": "Eggs and egg products, such as custard"
                  },
                  {
                    "id": 3,
                    "name": "Nut products"
                  },
                  {
                    "id": 4,
                    "name": "Cow's milk and milk products"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Do not force to feed your baby. Your baby will let you when he or she is full. If your baby leans back in the chair, turns his or her head away from food, starts playing with the spoon, or refuses to open up his or her mouth for the next bite, he or she has probably had enough.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Many foods will change the color and consistency of your infants stool. Some foods may make your baby's stool hard. If some foods cause constipation, such as rice cereal, bananas, or applesauce, switch to other fruits or vegetables or oatmeal or barley cereal.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Finger foods can be introduced around 9 months of age",
                "list": []
              },
            ]
          },
          {
            "id": 2,
            "name": "Colic",
            "class": "info",
            "desc": "Colic is a term used by medical...",
            "lt": "C",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Colic is a term used by medical professionals when babies cry for 3hours a day on more than 3 days a week. Bouts of colic usually start in the evening and occur more often during the first three months.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Colic usually goes away on its own when a baby is 3 or 4 months, however sometimes it can last a few months longer.",
                "list": []
              },
              {
                "title": "What can make babies cry besides colic?",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Hunger"
                  },
                  {
                    "id": 2,
                    "name": "Temperature – too hot or too cold"
                  },
                  {
                    "id": 3,
                    "name": "Too tired"
                  },
                  {
                    "id": 4,
                    "name": "Hurt"
                  },
                  {
                    "id": 5,
                    "name": "Sick"
                  },
                  {
                    "id": 6,
                    "name": "Allergy"
                  },
                ]
              },
              {
                "title": "What can I do to stop my baby crying…here are some tips",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Give your baby a warm bath"
                  },
                  {
                    "id": 2,
                    "name": "Swaddle – hip healthy swaddling"
                  },
                  {
                    "id": 3,
                    "name": "Massage has your baby sit up during feeds"
                  },
                  {
                    "id": 4,
                    "name": "Go for a car ride"
                  },
                  {
                    "id": 5,
                    "name": "Carry your baby in your arms or a sling"
                  },
                ]
              },
              {
                "title": "In case of allergy change the formula or avoid eating certain foods. Consult your doctor before trying this.",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "White noise"
                  },
                  {
                    "id": 2,
                    "name": "When to call my Doctor"
                  },
                  {
                    "id": 3,
                    "name": "Baby has a fever"
                  },
                  {
                    "id": 4,
                    "name": "You suspect your baby has been hurt or has been shaken too hard"
                  },
                  {
                    "id": 5,
                    "name": "Your baby is not responding normally"
                  },
                  {
                    "id": 6,
                    "name": "Your baby refuses to drink or eat and vomits or passes bloody stool"
                  },
                  {
                    "id": 7,
                    "name": "When you need a break from crying"
                  },
                  {
                    "id": 8,
                    "name": "Put your baby in a crib"
                  },
                  {
                    "id": 9,
                    "name": "Call a friend or a relative"
                  },
                  {
                    "id": 10,
                    "name": "Never hit shake or hurt your baby as it can cause brain injury"
                  },
                ]
              },
            ]
          },
          {
            "id": 3,
            "name": "The Common Cold",
            "class": "success",
            "desc": "This is the commonest infections...",
            "lt": "T",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "This is the commonest infections of the upper respiratory tract acquired in children. It is self-limiting and involves a wide range of symptoms. More often than not only supportive care is required for the treatment of a common cold. There are more than 100 different viruses responsible for this acute infection and therefore children can have multiple colds in a year",
                "list": []
              },
              {
                "title": "Sign and symptoms",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Nasal congestion"
                  },
                  {
                    "id": 2,
                    "name": "Clear yellow or green nasal discharge"
                  },
                  {
                    "id": 3,
                    "name": "Fever 38 degrees"
                  },
                  {
                    "id": 4,
                    "name": "Sore throat"
                  },
                  {
                    "id": 5,
                    "name": "Cough"
                  },
                  {
                    "id": 6,
                    "name": "Poor appetite"
                  },
                  {
                    "id": 7,
                    "name": "Symptoms are worse in the first 10 days"
                  },
                ]
              },
              {
                "title": "Treatment",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Fever – see section on fever treatment"
                  },
                  {
                    "id": 2,
                    "name": "Nasal saline drops with a bulb syringe to thin the mucus and clear the nose respectively"
                  },
                  {
                    "id": 3,
                    "name": "Humidified air may be tried"
                  },
                  {
                    "id": 4,
                    "name": "Plenty of liquids"
                  },
                  {
                    "id": 5,
                    "name": "Antibiotics – not effective for treating a common cod and do not prevent complications"
                  },
                ]
              },
              {
                "title": "Common cold complications",
                "paragraph": "Complications of a common cold may necessitate the use of an antibiotic. If fever persist or arises after 3 days of the common cold then please see your pediatrician.",
                "list": [{
                    "id": 1,
                    "name": "Ear infections"
                  },
                  {
                    "id": 2,
                    "name": "Sinusitis"
                  },
                  {
                    "id": 3,
                    "name": "Pneumonia"
                  },
                  {
                    "id": 4,
                    "name": "Asthma"
                  },
                  {
                    "id": 7,
                    "name": "Symptoms are worse in the first 10 days"
                  },
                ]
              },
            ]
          },
          {
            "id": 4,
            "name": "Diaper rash",
            "class": "danger",
            "desc": "This is a skin rash that happ...",
            "lt": "D",
            "files":[],
            "details": [{
                "title": "What is a diaper rash?",
                "paragraph": "This is a skin rash that happens anywhere in the area covered by the diaper. Diaper rashes are very common and can occur in any child wearing a diaper",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Good news: Most diaper rashes can be treated at home and usually subside in a few days.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "What causes diaper rash",
                "list": [{
                    "id": 1,
                    "name": "Contact with urine and bowel movement in the diaper. These are skin irritants and worsen when the child has diarrhea"
                  },
                  {
                    "id": 2,
                    "name": "Antibiotics"
                  },
                  {
                    "id": 3,
                    "name": "Perfumes or dyes in the diaper"
                  },
                  {
                    "id": 4,
                    "name": "Certain skin conditions can cause diaper rashes"
                  },
                  {
                    "id": 5,
                    "name": "Perfumed wipes"
                  },
                ]
              },
              {
                "title": "How does a diaper rash look like?",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Red painful and itchy"
                  },
                  {
                    "id": 2,
                    "name": "Raised peeling or scaly"
                  },
                  {
                    "id": 3,
                    "name": "Yellow blisters"
                  },
                  {
                    "id": 4,
                    "name": "If a diaper rash is caused by other skin conditions the rash can appear on other body parts"
                  },
                ]
              },
              {
                "title": "What can I do to make the rash better?",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Off diaper time as much as possible."
                  },
                  {
                    "id": 2,
                    "name": "Change the diaper ever 2 to 3 hours to keep it dry"
                  },
                  {
                    "id": 3,
                    "name": "Do not use wipes. Use warm water and cotton balls or a clean cloth"
                  },
                  {
                    "id": 4,
                    "name": "Use barrier creams to prevent worsening of the rash. Use zinc and castor cream"
                  },
                  {
                    "id": 5,
                    "name": "When to call my doctor"
                  },
                  {
                    "id": 6,
                    "name": "If rash worsens over time despite above interventions"
                  },
                  {
                    "id": 7,
                    "name": "If rash persists for more than 3 days without improvement"
                  },
                ]
              },
            ]
          },
          {
            "id": 5,
            "name": "Eczema (Atopic Dermatitis)",
            "desc": "This is skin condition that ma ",
            "class": "primary",
            "lt": "E",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "This is skin condition that makes the skin itchy and flaky. The exact cause of eczema is still unknown however it has been see in people who have allergies and can also be genetic.",
                "list": []
              },
              {
                "title": "How does eczema present",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Intense itching"
                  },
                  {
                    "id": 2,
                    "name": "Redness"
                  },
                  {
                    "id": 3,
                    "name": "Small bumps"
                  },
                  {
                    "id": 4,
                    "name": "Flaky skin"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Usually presents in children before the age of 5 years",
                "list": []
              },
              {
                "title": "Infants",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Cheeks"
                  },
                  {
                    "id": 2,
                    "name": "Legs and arms"
                  },
                ]
              },
              {
                "title": "Older children",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Neck"
                  },
                  {
                    "id": 2,
                    "name": "Elbow"
                  },
                  {
                    "id": 3,
                    "name": "Back of the knees"
                  },
                  {
                    "id": 4,
                    "name": "Skin can become dark and thick from intense scratching"
                  },
                ]
              },
              {
                "title": "How can I minimize the symptoms?",
                "paragraph": "",
                "list": []
              },
              {
                "title": "Remove factors that make the eczema worse",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "heat, perspiration and dry environments"
                  },
                  {
                    "id": 2,
                    "name": "rapid temperature changes"
                  },
                  {
                    "id": 3,
                    "name": "Exposure to chemicals or cleaning solutions such as detergents perfumes cosmetic lotions, smoke dust sand and wool or synthetic fibres."
                  },
                ]
              },
              {
                "title": "Hydration of the skin",
                "paragraph": "Emollients",
                "list": [{
                    "id": 1,
                    "name": "These are creams that moisturize the skin and prevent it from drying out. The best emollients are those which contain little or no water."
                  },
                  {
                    "id": 2,
                    "name": "The best times to apply this emollient are immediately after bathing"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Bathing",
                "list": [{
                    "id": 1,
                    "name": "Lukewarm baths can hydrate and cool the skin temporarily relieving the itching of the skin"
                  },
                  {
                    "id": 2,
                    "name": "Use an unscented mild soap or a non-soap cleanser"
                  },
                  {
                    "id": 3,
                    "name": "Hot or long baths greater than 15 minutes should be avoided as they tend to dry out the ski"
                  },
                ]
              },
              {
                "title": "Treatment",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Consult your doctor before using any of the products below"
                  },
                  {
                    "id": 2,
                    "name": "Topical steroids – prescription creams may be recommended to control mild to moderate dermatitis. Steroids are usually applied once or twice daily"
                  },
                  {
                    "id": 3,
                    "name": "Antihistamines – this usually helps to relieve itching"
                  },
                  {
                    "id": 4,
                    "name": "Wet dressing – help to soothe and hydrate the skin reduce itching and redness and prevent skin injury from scratching."
                  },
                  {
                    "id": 5,
                    "name": "Other treatments include oral steroids, tacrolimus cream U.V therapy and immunosuppressive drugs. However this will be decided by your physician based on the severity of the eczema"
                  },
                ]
              },
              {
                "title": "For more queries call the clinic line",
                "paragraph": "",
                "list": []
              },
            ]
          },
        ],
        "desc2": [{
            "id": 6,
            "name": "Fever",
            "class": "primary",
            "desc": "Fever is a normal response to...",
            "lt": "F",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Fever is a normal response to a variety of conditions. The commonest cause being infection. The challenge for parents is to know when to be concerned and how to appropriately manage fever",
                "list": []
              },
              {
                "title": "",
                "paragraph": "What is the definition of a fever?",
                "list": []
              },
              {
                "title": "",
                "paragraph": "There is no single value that defines a fever. Various areas on the body can be measured to give a temperature reading.",
                "list": [],
                "tablehead": [{
                    "id": 1,
                    "name": "Area"
                  },
                  {
                    "id": 2,
                    "name": "Temperature"
                  }
                ],
                "tablebody": [{
                    "id": 1,
                    "calss": "odd",
                    "area": "Rectal (most accurate)",
                    "temp": "38 degrees / 100.4 F"
                  },
                  {
                    "id": 2,
                    "calss": "even",
                    "area": "Axilla",
                    "temp": "38 degrees"
                  },
                  {
                    "id": 3,
                    "calss": "odd",
                    "area": "Oral",
                    "temp": "37.8 degrees/100 F"
                  },
                  {
                    "id": 4,
                    "calss": "even",
                    "area": "Forehead",
                    "temp": "38 degrees"
                  },
                  {
                    "id": 5,
                    "calss": "odd",
                    "area": "Ear",
                    "temp": "	38 degrees"
                  },
                ],
              },
              {
                "title": "Fever treatment options",
                "paragraph": "",
                "list": []
              },
              {
                "title": "Medications",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "TAcetaminophen (Panadol, calpol, Tylenol)"
                  },
                  {
                    "id": 2,
                    "name": "Given every 4 – 6 hours"
                  },
                  {
                    "id": 3,
                    "name": "Ibuprofen"
                  },
                  {
                    "id": 4,
                    "name": "Given every 6- 8 hours"
                  },
                  {
                    "id": 5,
                    "name": "Dosage"
                  },
                  {
                    "id": 6,
                    "name": "This is calculated according your child’s weight"
                  },
                ]
              },
              {
                "title": "Increased fluids",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Fever causes dehydration. Parents should encourage their children to drink as much fluid as possible."
                  },
                  {
                    "id": 2,
                    "name": "Children may not feel hungry and therefore it is not necessary to force them to eat."
                  },
                ]
              },
              {
                "title": "Rest",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Sponging and baths"
                  },
                  {
                    "id": 2,
                    "name": "Sponging is not as effective as medication and should not be used"
                  },
                  {
                    "id": 3,
                    "name": "Alcohol should not be used for sponging as it can cause increased risk of toxicity."
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Consult your pediatrician if",
                "list": [{
                    "id": 1,
                    "name": "Fever greater than 38 degrees"
                  },
                  {
                    "id": 2,
                    "name": "Child appears ill or fussy irrespective of the temperature"
                  },
                  {
                    "id": 3,
                    "name": "Child with a febrile convulsion or a history of the same"
                  },
                  {
                    "id": 4,
                    "name": "Children with a chronic medical problem"
                  },
                ]
              },

            ]
          },
          {
            "id": 7,
            "name": "Viral gastroenteritis",
            "class": "danger",
            "desc": "Viral gastroenteritis is an infectio...",
            "lt": "V",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Viral gastroenteritis is an infection that causes diarrhea and vomiting. The commonest cause of viral gastroenteritis is Rota virus. Children can get infected if they touch an infected person or a surface with the virus on it and then they don’t wash their hands, and secondly if they eat foods or drink liquids with the virus.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "How does it present",
                "list": [{
                    "id": 1,
                    "name": "Fever"
                  },
                  {
                    "id": 2,
                    "name": "Abdominal pain"
                  },
                  {
                    "id": 3,
                    "name": "Diarrhea"
                  },
                  {
                    "id": 4,
                    "name": "Vomiting"
                  },
                  {
                    "id": 5,
                    "name": "Loss of appetite"
                  },
                  {
                    "id": 6,
                    "name": "Dehydration"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "This is when the body loses too much water.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Dehydration can make you thirsty, tired, dizzy and reduced urine output",
                "list": []
              },
              {
                "title": "How can I help my child feel better?",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Consult your doctor"
                  },
                  {
                    "id": 2,
                    "name": "Give ORS"
                  },
                  {
                    "id": 3,
                    "name": "Avoid juice or soda as it can worsen the diarrhea"
                  },
                  {
                    "id": 4,
                    "name": "Eat whole grain breads and cereals fruits and vegetable"
                  },
                  {
                    "id": 5,
                    "name": "Avoid eating foods with too much sugar and fat."
                  },
                ]
              },
              {
                "title": "See the doctor immediately if",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Symptoms of dehydration"
                  },
                  {
                    "id": 2,
                    "name": "Diarrhea and vomiting lasting for a few days"
                  },
                  {
                    "id": 3,
                    "name": "Vomits blood or bloody diarrhea"
                  },
                  {
                    "id": 4,
                    "name": "Refusal to drink"
                  },
                  {
                    "id": 5,
                    "name": "Has not passed urine in the past 8 hours"
                  },
                ]
              },
              {
                "title": "Treatment",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Usually it is self-limiting; however the doctor may give you medicines to make your child feel better"
                  },
                  {
                    "id": 2,
                    "name": "If your child is dehydrated you may require to be admitted for intravenous fluids."
                  },
                ]
              },
              {
                "title": "Prevention",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Always encourage your child to wash hands with soap and water after using the bathroom, or before you eat. As a mother ensure to wash your hands after changing the diaper and before feeding your baby."
                  },
                  {
                    "id": 2,
                    "name": "Rota virus vaccine"
                  },
                ]
              },
            ]
          },
          {
            "id": 8,
            "name": "Sleep Training",
            "class": "info",
            "desc": "Travelling for a holiday...",
            "lt": "S",
            "files":[],
            "details": [{
                "title": "Sleeping tips for your child",
                "paragraph": "Introduction",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Sleep is as important as food and water. Although this may seem apparent but many of us don’t allow our children to get the critical sleep they need to develop and function properly. Marc Weisbluth said in his book;",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Sleep is the power source that keeps your mind alert and calm. Every night and at every nap, sleep recharges the brain's battery. Sleeping well increases brainpower just as weight lifting builds stronger muscles, because sleeping well increases your attention span and allows you to be physically relaxed and mentally alert at the same time. Then you are at your personal best.",
                "list": []
              },
              {
                "title": "The essentials of sleeps are",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Sufficient amount of sleep"
                  },
                  {
                    "id": 2,
                    "name": "Uninterrupted sleep"
                  },
                  {
                    "id": 3,
                    "name": "The proper number of age appropriate naps"
                  },
                ]
              },
              {
                "title": "What do you need to do?",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Establish a regular nap schedules."
                  },
                  {
                    "id": 2,
                    "name": "Put your baby to sleep early 6.30 – 7.30. don’t fall into the trap of keeping a child awake so he/ she will be more tired. An overtired baby will have more difficulty falling asleep."
                  },
                  {
                    "id": 3,
                    "name": "If your baby is a late sleeper then make the changes slowly as this will be easier for the baby to adjust"
                  },
                  {
                    "id": 4,
                    "name": "Stick to a routine every single day and keep it short. Example ; bath, then a book then a lullaby then bed and this should be followed every single night"
                  },
                  {
                    "id": 5,
                    "name": "Create a comfortable sleeping environment"
                  },
                  {
                    "id": 6,
                    "name": "Don’t respond to every noise your child makes. Learn to differentiate a real cry from a sleepy whimper"
                  },
                ]
              },
              {
                "title": "First recognize sleep signals for your child",
                "paragraph": "When children get tired they have different ways of telling us they are tired and when you learn to identify these signals it will be a lot easier to put them to sleep. These are the most common sleep signals",
                "list": [{
                    "id": 1,
                    "name": "Tugging at ears"
                  },
                  {
                    "id": 2,
                    "name": "Rubbing of the nose"
                  },
                  {
                    "id": 3,
                    "name": "Fussy"
                  },
                  {
                    "id": 4,
                    "name": "A far away stare"
                  },
                  {
                    "id": 5,
                    "name": "Arching of the back"
                  },
                  {
                    "id": 6,
                    "name": "Sometimes if the child passes the sleep time they will get more happier and giddier as they tire more"
                  },
                ]
              },
              {
                "title": "Bedtime Routine",
                "paragraph": "",
                "list": []
              },
              {
                "title": "Why is it absolutely necessary to have a bedtime routine?",
                "paragraph": "Babies and toddlers thrive well on routines. They like to know what is going to happen next. Create a quick and a fun bed time routine that is less stress for you and your baby. With a bed time routine you need to create a series of steps that you will follow every single night so that your child is able to predict when she is going to fall asleep.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Make sure the routine is fun. You do not want your child to be angry. Do not do anything that your child hates doing.   All the steps in the bed time routine should be enjoyable to the child",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Keep the routine short say between 20 to 30 minutes. If you let the bed time drag for too long your child will be bored and get overtired and hence more difficult to fall asleep. Bedtime is a great time to bond with the child and you want your child to be calmed down relaxed and ready for sleep.",
                "list": []
              },

              {
                "title": "Example",
                "paragraph": "REMEMBER: do not let your child fall asleep anytime in between the routine",
                "list": []
              },
              {
                "title": "No TV or screen time or sugar before bed",
                "paragraph": "Screen time over stimulates the child so avoid using phones or television before bed time. Sugar will give them jittery energy and a sugar rush that will make it difficult for him to fall asleep.",
                "list": []
              },
              {
                "title": "Sleep props",
                "paragraph": "A sleep prop is anything your child needs to fall asleep such as soothers, breast feeding, bottle feeding. Remember it is only called a sleep prop if it helps your child to fall asleep.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "If you have identified what your child’s sleep prop is, try an experiment. At bed time do not give your child the sleep prop and see what happens give it 3 to 4 minutes if she cries for longer than that then let your child use whatever he has been using.",
                "list": []
              },
              {
                "title": "Don’t respond to every noises",
                "paragraph": "Don’t jump up to every noise of your child. Time your entrance as waiting a few minutes to soothe herself back to sleep and stepping in before she melts down you’ll catch her before she is too worked up to fall back asleep. Ten minutes is a good time allowance if you can hang in that long",
                "list": []
              },
              {
                "title": "Start dimming lights before bed",
                "paragraph": "Our internal clocks rely heavily on light to determine when it’s time to get ready to sleep. Start dimming or turning off lights gradually about an hour before bedtime, or slightly before you start baby’s bedtime routine.",
                "list": []
              },
              {
                "title": "Keep the baby’s bedroom boring",
                "paragraph": "Most parents are big fans of rocking, feeding and singing their babies to sleep, but this can be counter-productive. When baby wakes up at night, she’ll need you to repeat the behaviour so she can get back to sleep. Instead, put her down while she’s still awake and let her learn how to make the journey on her own.",
                "list": []
              },
              {
                "title": "Get outdoors",
                "paragraph": "Getting a baby outdoors during the day is a great way to help baby sleep. The sunlight helps in serotonin production which in turn is converted to melatonin which helps them sleep better.",
                "list": []
              },
              {
                "title": "Check the temperature",
                "paragraph": "Having a bedroom too cold or too warm may greatly affect how a child sleeps. Don’t overdress your child whilst sleeping. Studies have shown keeping a cool temperature enhances sleep better than a hot environment.",
                "list": []
              },
              {
                "title": "White noise",
                "paragraph": "A white noise machine sets at an appropriate volume can have a lot of difference. You can use the sound of ocean waves or a fan or a washing machine. (app on the smart phone for white noise)",
                "list": []
              },
              {
                "title": "Make a commitment",
                "paragraph": "Sleep is a necessity for both yourself and your child especially if you want to function in a happy healthy and a productive manner. It takes an effort to train them but the reward is immense.",
                "list": []
              },
            ]
          },
          {
            "id": 9,
            "name": "Car seat",
            "class": "success",
            "desc": "Travelling for a holiday...",
            "lt": "C",
            "files":[],
            "details": [{
                "title": "American Academy of Pediatric Guidelines (AAP)",
                "paragraph": "The AAP advises parents to keep their toddlers in rear facing car seats until the age of two or until they reach a maximum height and weight for the seat.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "It also advises that most children will need to ride in a belt position booster seat until they have reached 4 feet 9 inches tall and are between 8 to 12 years old",
                "list": []
              },
              {
                "title": "",
                "paragraph": "For further information on car seats and the types read the following link",
                "list": []
              },
              {
                "title": "",
                "paragraph": "https://www.healthychildren.org/English/safety-prevention/on-the-go/Pages/Car-Safety-Seats-Information-for-Families.aspx",
                "list": []
              },
              {
                "title": "Policy Statement—Child Passenger Safety pdf",
                "paragraph": "",
                "list": []
              },
            ]
          },
          {
            "id": 10,
            "name": "A Guide for Weaning Your Child",
            "desc": "Congratulations! you have reached...",
            "class": "warning",
            "lt": "A",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Congratulations ….you have reached yet another milestone for your baby",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Is my child ready to start solid foods?",
                "list": []
              },
              {
                "title": "",
                "paragraph": "When is the right time?",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Introducing your baby to solid foods is called weaning. The best time to start weaning is usually at 6 months of age; however the following signs should be present in your baby",
                "list": [{
                    "id": 1,
                    "name": "They can stay in a sitting position and hold their head steady"
                  },
                  {
                    "id": 2,
                    "name": "They can coordinate their hands eyes and mouth"
                  },
                  {
                    "id": 3,
                    "name": "They can swallow food. Babies who are not ready will push the food back out"
                  },
                ]
              },
              {
                "title": "Note:",
                "paragraph": "A baby wanting extra feeds or waking up if sleeping through the night previously is not a sign of readiness to wean.",
                "list": []
              },
              {
                "title": "Why wait till 6 months",
                "paragraph": "Research shows that babies need nothing more than breast milk or infant formula in the first 6 months of life. Breast milk will give extra protection against infection.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Breast milk provides all the nutrients that a baby needs for healthy growth and development in the first 6 months",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Breast fed babies may be less likely to become obese later in childhood",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Mothers who exclusively breastfeed are more like to their pre-pregnancy weight",
                "list": []
              },
              {
                "title": "Some interesting facts",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "At present 49% of mothers start solid foods by 4 months"
                  },
                  {
                    "id": 2,
                    "name": "It is common for mothers to believe that giving solid foods will help their baby to sleep longer at night"
                  },
                  {
                    "id": 3,
                    "name": "Many mothers would like their child to be ahead of its peers"
                  },
                ]
              },
              {
                "title": "Getting started",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Always stay with your baby when feeding in case they choke on the food"
                  },
                  {
                    "id": 2,
                    "name": "Allow your baby to touch the food and explore the texture …do not worry about the mess"
                  },
                  {
                    "id": 3,
                    "name": "As soon as your child shows interest allow them to feed themselves"
                  },
                  {
                    "id": 4,
                    "name": "If using a spoon wait for your baby to open the mouth"
                  },
                  {
                    "id": 5,
                    "name": "Cool hot food and test before you offer it to your child"
                  },
                  {
                    "id": 6,
                    "name": "Fresh food preferred over readymade pureed foods"
                  },
                  {
                    "id": 7,
                    "name": "Pass the cooked food through a strainer so that there are no lumps"
                  },
                  {
                    "id": 8,
                    "name": "Gradually thicken the puree introduce lumpier and mash foods as you progress"
                  },
                ]
              },
              {
                "title": "DONOT FORCE YOUR CHILD TO EAT",
                "paragraph": "",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Weaning food chart",
                "list": [],
              },
              {
                "title": "6 MONTHS First foods",
                "paragraph": "CHILD CAN HAVE.",
                "list": [{
                    "id": 1,
                    "name": "Baby rice"
                  },
                  {
                    "id": 2,
                    "name": "Yellow / orange fruit and vegetable purees (babies can digest the above more easily than green vegetables)"
                  },
                  {
                    "id": 3,
                    "name": "Lentils"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "CAN’T HAVE",
                "list": [{
                    "id": 1,
                    "name": "Gluten (avoid wheat, rye and barley-based foods e.g. bread, flour, pasta, some breakfast cereals, rusks and oats.)"
                  },
                  {
                    "id": 2,
                    "name": "Eggs"
                  },
                  {
                    "id": 3,
                    "name": "Nuts and sesame foods (like sesame oil)"
                  },
                  {
                    "id": 4,
                    "name": "Fish or shellfish"
                  },
                  {
                    "id": 5,
                    "name": "Soya-based products"
                  },
                  {
                    "id": 6,
                    "name": "Honey"
                  },
                  {
                    "id": 7,
                    "name": "Cows, goats and sheep's milk"
                  },
                  {
                    "id": 8,
                    "name": "Soft and unpasteurised cheeses"
                  },
                  {
                    "id": 9,
                    "name": "Chicken or meat"
                  },
                  {
                    "id": 10,
                    "name": "Salt"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "APPROX QUANTITIES",
                "list": [{
                    "id": 1,
                    "name": "Approx 2 teaspoons per meal. Start with 1 meal a day and slowly increasing to 3 meals a day"
                  },
                  {
                    "id": 2,
                    "name": "800 – 1000 ml of breast / infant formula"
                  },
                ]
              },
              {
                "title": "6-7 MONTHS",
                "paragraph": "CHILD CAN HAVE.",
                "list": [{
                    "id": 1,
                    "name": "As above plus"
                  },
                  {
                    "id": 2,
                    "name": "all fruits and vegetables mashed"
                  },
                  {
                    "id": 3,
                    "name": "oats and other gluten foods"
                  },
                  {
                    "id": 4,
                    "name": "chicken puree"
                  },
                  {
                    "id": 5,
                    "name": "fish"
                  },
                  {
                    "id": 6,
                    "name": "cheese"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "CAN’T HAVE",
                "list": [{
                    "id": 1,
                    "name": "Honey"
                  },
                  {
                    "id": 2,
                    "name": "Salt"
                  },
                  {
                    "id": 3,
                    "name": "Shark, swordfish or marlin (due to high mercury levels)"
                  },
                  {
                    "id": 4,
                    "name": "Goats and sheep's milk"
                  },
                  {
                    "id": 5,
                    "name": "Mould ripened soft cheeses"
                  },
                  {
                    "id": 6,
                    "name": "Raw or lightly cooked eggs"
                  },
                  {
                    "id": 7,
                    "name": "Sugary foods and drinks (these can encourage a sweet tooth and lead to tooth decay when your baby’s teeth start to come through. Only add sugar to foods if it’s really necessary. Sweet puddings, biscuits, sweets and ice creams are not recommended."
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "APPROX QUANTITIES",
                "list": [{
                    "id": 1,
                    "name": "Approx 4-6 teaspoons per meal."
                  },
                  {
                    "id": 2,
                    "name": "500 – 600 ml of breast milk/ infant formula"
                  },
                ]
              },
              {
                "title": "9-12 MONTHS",
                "paragraph": "CHILD CAN HAVE.",
                "list": [{
                    "id": 1,
                    "name": "As above plus"
                  },
                  {
                    "id": 2,
                    "name": "Whole egg"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "CAN’T HAVE",
                "list": [{
                  "id": 1,
                  "name": "As above"
                }, ]
              },
              {
                "title": "",
                "paragraph": "APPROX QUANTITIES",
                "list": [{
                    "id": 1,
                    "name": "12 teaspoons per meal"
                  },
                  {
                    "id": 2,
                    "name": "mid morning and afternoon snack"
                  },
                  {
                    "id": 3,
                    "name": "500 ml of breast / infant formula"
                  },
                ]
              },

              {
                "title": "12 MONTHS PLUS",
                "paragraph": "CHILD CAN HAVE.",
                "list": [{
                    "id": 1,
                    "name": "As above plus"
                  },
                  {
                    "id": 2,
                    "name": "Honey"
                  },
                  {
                    "id": 3,
                    "name": "Cows milk to drink"
                  },
                  {
                    "id": 4,
                    "name": "Limited salt"
                  },
                  {
                    "id": 5,
                    "name": "Limited mould ripen cheese"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "CAN’T HAVE",
                "list": [{
                  "id": 1,
                  "name": "whole nuts (choking hazard)"
                }, ]
              },
              {
                "title": "",
                "paragraph": "APPROX QUANTITIES",
                "list": [{
                    "id": 1,
                    "name": "Approx 250 gms per meal"
                  },
                  {
                    "id": 2,
                    "name": "2 healthy snack times"
                  },
                  {
                    "id": 3,
                    "name": "300 – 400 ml milk (full fat cows milk"
                  },
                ]
              },

              {
                "title": "Ideas on different foods you can give to your child",
                "paragraph": "*indicates take with caution especially if prone to allergies. Please consult.",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Accessories you may need for feeding your child",
                "list": [{
                    "id": 1,
                    "name": "Splash mat – for the inevitable spills"
                  },
                  {
                    "id": 2,
                    "name": "Weaning spoons – rubber tipped spoons helps with the baby’s sensitive gums"
                  },
                  {
                    "id": 3,
                    "name": "High chair – use once the baby can sit up. Develops good eating habits"
                  },
                  {
                    "id": 4,
                    "name": "Selection of bibs – weaning is messy, it will help protect the baby’s clothes."
                  },
                  {
                    "id": 5,
                    "name": "Sieve and a spoon – to avoid lumps in the food"
                  },
                  {
                    "id": 6,
                    "name": "A free flowing beaker – introduce a cup from 6 months and give sips of water with every meal. This will help your baby learn to sip and also protect the teeth"
                  },
                ]
              },
              {
                "title": "Questions you may want to ask your paediatrician",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "What do I do if weaning is a struggle?"
                  },
                  {
                    "id": 2,
                    "name": "Will my child get enough nutrients?"
                  },
                  {
                    "id": 3,
                    "name": "Do I still need to breast feed?"
                  },
                  {
                    "id": 4,
                    "name": "My baby’s poo has changed is it normal?"
                  },
                  {
                    "id": 5,
                    "name": "Baby’s new food diary"
                  },
                ]
              },
            ]
          }
        ],
      },
    },
    {
      "id": 5,
      "icon": "hospital.svg",
      "servicedone": "Travel tips",
      "doctor": "Dr. Farida Essajee.",
      "deacription": {
        "desc1": [{
            "id": 1,
            "name": "Sun safety tips",
            "class": "info",
            "desc": "Travelling for a holiday...",
            "lt": "S",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Travelling for a holiday?   Here are some tips to avoid a sunburn",
                "list": []
              },
              {
                "title": "",
                "paragraph": "A sunburn happens when the skin gets burnt from invisible light called the ultraviolet light and this happens when exposed to the sunlight for a long duration. It is important to avoid a sunburn as people who get regularly sunburnt have a risk of other problems.",
                "list": [{
                    "id": 1,
                    "name": "Skin cancer"
                  },
                  {
                    "id": 2,
                    "name": "Wrinkles"
                  },
                  {
                    "id": 2,
                    "name": "Eye problems – e.g. cataracts"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "You are at a higher risk if your skin color is lighter or you are on medicines that can make the skin burn more easily",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Sun safety tips",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Infants and young children",
                "list": [{
                    "id": 1,
                    "name": "The American academy of pediatrics recommends avoiding the use of sunscreen products in infants younger than six months"
                  },
                  {
                    "id": 2,
                    "name": "When adequate clothing or a shade is not available a minimal amount of sunscreen with at least 15 SPF can be applied to small areas e.g. infants face and hands"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Older children",
                "list": [{
                    "id": 1,
                    "name": "Staying out of the sun in the middle of the day"
                  },
                  {
                    "id": 2,
                    "name": "Staying under a sun umbrella or a shade"
                  },
                  {
                    "id": 3,
                    "name": "Wearing sunscreen – apply sunscreen to all parts of the body not covered by clothes. Reapply after every 3 hours or after you sweat or swim"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Choose your sunscreen wisely",
                "list": [{
                    "id": 1,
                    "name": "SPF of 30 or greater"
                  },
                  {
                    "id": 2,
                    "name": "Protects against 2 types of UV light UVA and UVB. (broad spectrum)"
                  },
                  {
                    "id": 2,
                    "name": "Sunscreen has not expired or over 3 years old"
                  },
                  {
                    "id": 2,
                    "name": "Lip balm with SPF 30"
                  },
                  {
                    "id": 2,
                    "name": "Long clothing whenever possible"
                  },
                ]
              },
            ]
          },
          {
            "id": 2,
            "name": "Insect bites",
            "class": "primary",
            "desc": "What to do when you are stung...",
            "lt": "I",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "What to do when you are stung by a bee or a wasp or any insect that can sting",
                "list": []
              },
              {
                "title": "Step 1 – remove the stinger from your skin immediately",
                "paragraph": "",
                "list": []
              },
              {
                "title": "Step 2 – the area around the sting can swell hurt and become red",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Wash the stung area with water and soap"
                  },
                  {
                    "id": 2,
                    "name": "Keep the area clean"
                  },
                  {
                    "id": 2,
                    "name": "Do not scratch"
                  },
                  {
                    "id": 2,
                    "name": "Apply a cold pack or a cool cloth over the area"
                  },
                  {
                    "id": 2,
                    "name": "Apply antiitch medicine"
                  },
                  {
                    "id": 2,
                    "name": "Pain killers"
                  },
                ]
              },
              {
                "title": "Step 3 – if an allergic reaction develops to the sting. Watch out for the following signs",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Difficulty in breathing"
                  },
                  {
                    "id": 2,
                    "name": "Swelling round the face"
                  },
                  {
                    "id": 2,
                    "name": "Belly cramps, nausea vomiting or diarrhea"
                  },
                  {
                    "id": 2,
                    "name": "Loss of consciousness"
                  },
                ]
              },
              {
                "title": "Severe allergic reaction treatment",
                "paragraph": "",
                "list": [{
                    "id": 1,
                    "name": "Severe allergic reactions are a medical emergency and if not treated immediately can lead to death"
                  },
                  {
                    "id": 2,
                    "name": "Call your doctor immediately"
                  },
                  {
                    "id": 2,
                    "name": "Give the epi pen shot immediately"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "How to avoid stings",
                "list": [{
                    "id": 1,
                    "name": "Avoid walking bare feet"
                  },
                  {
                    "id": 2,
                    "name": "Do not threaten bees or wasp or else they will be sure to sting"
                  },
                  {
                    "id": 2,
                    "name": "If a stinging insect is close by slowly back away and do not flail your arms. If you are being swammed or stung cover your mouth and nose with you hand and run inside a building or vehicle"
                  },
                  {
                    "id": 2,
                    "name": "For outdoor activities wear insect repellent."
                  },
                ]
              },
            ]
          },
          {
            "id": 3,
            "name": "Travel tips",
            "class": "danger",
            "desc": "Travelling with your child ...",
            "lt": "T",
            "files":[],
            "details": [{
                "title": "",
                "paragraph": "Travelling with your child require planning and precaution in order for you to have an enjoyable experience",
                "list": []
              },
              {
                "title": "",
                "paragraph": "Most of the travel related health problems are preventable and do pay us a visit for your pre travel evaluation",
                "list": []
              },
              {
                "title": "Pre – Travel Evaluation",
                "paragraph": "This visit to the clinic will provide us with the following details",
                "list": [{
                    "id": 1,
                    "name": "Duration of the travel"
                  },
                  {
                    "id": 2,
                    "name": "Season of travel"
                  },
                  {
                    "id": 3,
                    "name": "Countries visited"
                  },
                  {
                    "id": 4,
                    "name": "Planned activates"
                  },
                  {
                    "id": 5,
                    "name": "Place of residence"
                  },
                ]
              },
              {
                "title": "Travel related advice",
                "paragraph": "Vaccines required",
                "list": [{
                    "id": 1,
                    "name": "This will depend to your country of travel"
                  },
                  {
                    "id": 2,
                    "name": "Contact us for further information"
                  },
                ]
              },

              {
                "title": "",
                "paragraph": "Food and water precautions",
                "list": [{
                    "id": 1,
                    "name": "Do not drink unboiled tap water"
                  },
                  {
                    "id": 2,
                    "name": "Do not drink beverages that contain ice from unboiled tap water"
                  },
                  {
                    "id": 3,
                    "name": "Drink boiled tap water"
                  },
                  {
                    "id": 4,
                    "name": "Do not eat raw or rare meats"
                  },
                  {
                    "id": 5,
                    "name": "Do not eat raw vegetables"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Insect and tick bites",
                "list": [{
                    "id": 1,
                    "name": "Buy insect repellents"
                  },
                  {
                    "id": 2,
                    "name": "Wear protective clothing, including long sleeved shirts"
                  },
                  {
                    "id": 3,
                    "name": "Whenever possible minimize outdoor time spent after night fall."
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Malaria prevention",
                "list": [{
                    "id": 1,
                    "name": "Malaria is an infection spread by the bite of a mosquito"
                  },
                  {
                    "id": 2,
                    "name": "Recommended for people travelling from foreign countries to kenya especially coastal and western province"
                  },
                  {
                    "id": 3,
                    "name": "Malaria medications will be prescribed at the clinic"
                  },
                ]
              },
              {
                "title": "",
                "paragraph": "Sun screen lotions",
                "list": [{
                  "id": 1,
                  "name": "Check section on sun safety tips."
                }, ]
              },
            ]
          },
        ],
      },
    },
    {
      "id": 6,
      "icon": "babystroller.svg",
      "servicedone": "Courses",
      "doctor": "Dr. Anne-Marie.",
      "deacription": {
        "desc1": [{
          "id": 1,
          "name": "Basic Life Support: Child and Infant",
          "desc": "This is a short course in basic ...",
          "class": "primary",
          "lt": "B",
          "files":[],
          "details": [{
              "title": "Overview",
              "paragraph": "This is a short course in basic life support provided by the clinic to our parents. The aim of the course is to enable you as parents to deal with an unconscious and a choking child.",
              "list": []
            },
            {
              "title": "Suitability",
              "paragraph": "This course is suitable for anyone from any industry or occupation who wishes to learn how to resuscitate in the unfortunate event that this may be necessary. This course is particularly relevant to parents and careers, child minders or anyone working in the industry",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The content of this course complies with the European resuscitations guidelines.",
              "list": []
            },
            {
              "title": "Fee Structure",
              "paragraph": "3000 shillings per couple",
              "list": []
            },
            {
              "title": "Duration of course",
              "paragraph": "Two hours (4-6pm)",
              "list": []
            },
            {
              "title": "Course content",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Lecture on BLS and choking"
                },
                {
                  "id": 2,
                  "name": "Questions"
                },
                {
                  "id": 3,
                  "name": "Scene assessment"
                },
                {
                  "id": 4,
                  "name": "Scenarios"
                },
                {
                  "id": 5,
                  "name": "Unconscious child"
                },
                {
                  "id": 5,
                  "name": "Choking child"
                },
                {
                  "id": 5,
                  "name": "Tea"
                },
              ]
            },
            {
              "title": "",
              "paragraph": "Our trainers will bring along resuscitation dolls appropriate to the ages of the children, to help demonstrate the actions needed. Every person in the class will be given ample opportunity to practice and boost their confidence.",
              "list": []
            },
            {
              "title": "Reading material",
              "paragraph": "",
              "list": []
            },
            {
              "title": "What is basic life support?",
              "paragraph": "BLS is a sequence of events that need to be undertaken to initially try to revive a collapsed person. It can be performed by anyone who has been trained to do so, and in almost any setting. If started as soon as the person requires it, BLS provides the best possible chance of a good outcome for the collapsed infant or child.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The key points in BLS are:",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The key points in BLS are:",
              "list": [{
                  "id": 1,
                  "name": "Airway"
                },
                {
                  "id": 2,
                  "name": "Breathing"
                },
                {
                  "id": 3,
                  "name": "Circulation"
                },
                {
                  "id": 4,
                  "name": "ABC - for short."
                },
              ]
            },
            {
              "title": "",
              "paragraph": "The first two steps of BLS – Airway and Breathing – deal with respiratory (breathing) arrest and the third step of BLS – Circulation – is intended to deal with cardiac (heart) arrest. However, this third stage will only be effective if Airway and Breathing are dealt with first. In other words, A needs to be dealt with before B, which needs to be dealt with before C.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "BLS for babies and children differ in some important ways from that for adults and therefore require specific training for it to be effectively delivered.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The most important difference is that BLS for babies and children is mainly required for a respiratory (or breathing) emergency, while in adults, it is more likely that they will have a cardiac (or heart) problem. This means in the majority of babies and children, starting adequate BLS as soon as necessary may require only the breathing part to be given, as their heart will not be particularly affected.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "However, as damage to the brain and other vital organs (such as the kidneys and heart muscle) starts to occur after just a few minutes of being short of oxygen, it is essential that when needed BLS is started immediately. Therefore, any person who suspects a baby or child is not breathing adequately, must be able to assess the need for, and start BLS without delay.",
              "list": []
            },
            {
              "title": "Instructions:",
              "paragraph": "",
              "list": []
            },
            {
              "title": "Check the area for DANGER to yourself or others",
              "paragraph": "",
              "list": []
            },
            {
              "title": "Check your baby for any response",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Infants and children should never be shaken"
                },
                {
                  "id": 2,
                  "name": "Tap the child or firmly ask ‘are you ok’"
                },
                {
                  "id": 3,
                  "name": "If no response"
                },
              ]
            },
            {
              "title": "Send for Help",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Call for an ambulance"
                },
                {
                  "id": 2,
                  "name": "Call your pediatrician (0721967818)"
                },
                {
                  "id": 3,
                  "name": "A bystander can do this for you"
                },
              ]
            },
            {
              "title": "Open airway - A",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Lie baby on the back"
                },
                {
                  "id": 2,
                  "name": "Support the lower jaw at the point of the chin with your fingers"
                },
                {
                  "id": 3,
                  "name": "Open the mouth"
                },
                {
                  "id": 4,
                  "name": "Check if airway is clear"
                },
              ]
            },
            {
              "title": "Open airway - A",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Lie baby on the back"
                },
                {
                  "id": 2,
                  "name": "Support the lower jaw at the point of the chin with your fingers"
                },
                {
                  "id": 3,
                  "name": "Open the mouth"
                },
                {
                  "id": 4,
                  "name": "Check if airway is clear"
                },
              ]
            },
            {
              "title": "Assess breathing – B",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "Look listen and feel for any breath sounds"
                },
                {
                  "id": 2,
                  "name": "If not breathing well – give 5 rescue breaths"
                },
              ]
            },
            {
              "title": "",
              "paragraph": "Place your mouth over the baby’s mouth and nose ensuring a good seal",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Give 5 rescue breaths, each breath should be enough to make your baby’s chest rise and fall.",
              "list": []
            },
            {
              "title": "If not breathing start CPR – cardiopulmonary resuscitation - C",
              "paragraph": "CPR is a way to get blood and oxygen moving throughout the body of someone whose heart has stopped working. CPR can save a person’s life. It can keep the brain and other organs from being damaged by lack of oxygen.",
              "list": [{
                  "id": 1,
                  "name": "View the centre of the chest: place two fingers on the lower half of the breastbone (sternum)"
                },
                {
                  "id": 2,
                  "name": "Start chest compressions. Push the chest down by 1/3rd of the depth of the baby’s chest. Give 15 chest compressions and then give 2 breaths"
                },
                {
                  "id": 3,
                  "name": "For children more than 1 year old."
                },
              ]
            },
            {
              "title": "",
              "paragraph": "Place the heel of one hand over the lower half of the sternum",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Lift the fingers to ensure that pressure is not applied over the child’s ribs.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Position yourself vertically above the victim’s chest and, with your arm straight, compress the sternum to depress it by at least one-third of the depth of the chest. In larger children, or for small rescuers, this may be achieved most easily by using both hands with the fingers interlocked.",
              "list": []
            },
            {
              "title": "Continue with the resuscitation",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "The child shows signs of life (normal breathing, cough, movement or definite"
                },
                {
                  "id": 1,
                  "name": "Pulse of greater than 60 min)."
                },
                {
                  "id": 1,
                  "name": "Further qualified help arrives."
                },
                {
                  "id": 1,
                  "name": "You become exhausted."
                },
              ]
            },
            {
              "title": "",
              "paragraph": "Recovery position",
              "list": [{
                  "id": 1,
                  "name": "An unconscious child whose airway is clear and who is breathing normally should be turned onto his side into the recovery position. The important principles to be followed are:"
                },
                {
                  "id": 1,
                  "name": "The child should be placed in as near a true lateral position as possible with his mouth dependant to enable free drainage of fluid."
                },
                {
                  "id": 1,
                  "name": "The position should be stable. In an infant, this may require the support of a small pillow or a rolled-up blanket placed behind his back to maintain the position."
                },
              ]
            },
            {
              "title": "",
              "paragraph": "There should be no pressure on the chest that impairs breathing.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "It should be possible to turn the child onto his side and to return him back easily and safely, taking into consideration the possibility of cervical spine injury.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The airway should be accessible and easily observed.",
              "list": []
            },
            {
              "title": "Choking",
              "paragraph": "Choking occurs when a foreign object gets stuck in your baby’s trachea and is keeping air from flowing normally into an out of the lungs, so the child isn’t able to breathe properly. Choking can become a life threatening emergency as sometimes the object can completely obstruct the airway. If airflow into and out of the lungs is blocked and the brain is deprived of oxygen, choking can become a life threatening emergency.",
              "list": [{
                  "id": 1,
                  "name": "General signs of choking"
                },
                {
                  "id": 2,
                  "name": "Witnessed episode"
                },
                {
                  "id": 3,
                  "name": "Coughing or choking"
                },
                {
                  "id": 4,
                  "name": "Sudden onset"
                },
                {
                  "id": 5,
                  "name": "Recent history of playing with or eating small objects"
                },
              ]
            },
            {
              "title": "Relief of choking",
              "paragraph": "",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Safety is paramount. Rescuers should avoid placing themselves in danger and consider the safest action to manage the choking child:",
              "list": [{
                  "id": 1,
                  "name": "If the child is coughing effectively, then no external manoeuvre is necessary."
                },
                {
                  "id": 2,
                  "name": "Encourage the child to cough, and monitor continuously."
                },
                {
                  "id": 3,
                  "name": "If the child’s coughing is, or is becoming, ineffective, shout for help immediately and determine the child’s conscious level."
                },
              ]
            },
            {
              "title": "Conscious child with choking",
              "paragraph": "",
              "list": [{
                  "id": 1,
                  "name": "If the child is still conscious but has absent or ineffective coughing, give back blows."
                },
                {
                  "id": 1,
                  "name": "If back blows do not relieve choking, give chest thrusts to infants or abdominal thrusts to children. These maneuvers create an ‘artificial cough’ to increase intrathoracic pressure and dislodge the foreign body."
                },
              ]
            },
            {
              "title": "",
              "paragraph": "Back blows",
              "list": []
            },
            {
              "title": "In an infant:",
              "paragraph": "Support the infant in a head-downwards, prone position, to enable gravity to assist removal of the foreign body.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "A seated or kneeling rescuer should be able to support the infant safely across his lap. Support the infant’s head by placing the thumb of one hand at the angle of the lower jaw, and one or two fingers from the same hand at the same point on the other side of the jaw.",
              "list": []
            },

            {
              "title": "",
              "paragraph": "Do not compress the soft tissues under the infant’s jaw, as this will exacerbate the airway obstruction.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Deliver up to 5 sharp back blows with the heel of one hand in the middle of the back between the shoulder blades. The aim is to relieve the obstruction with each blow rather than to give all",
              "list": []
            },
            {
              "title": "In a child over 1 year:",
              "paragraph": "Back blows are more effective if the child is positioned head down.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "A small child may be placed across the rescuer’s lap as with an infant.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "If this is not possible, support the child in a forward-leaning position and deliver the back blows from behind.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "If back blows fail to dislodge the object, and the child is still conscious, use chest thrusts for infants or abdominal thrusts for children. Do not use abdominal thrusts (Heimlich manoeuvre) for infants.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Chest thrusts for infants:",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Turn the infant into a head-downwards supine position. This is achieved safely by placing your free arm along the infant’s back and encircling the occiput with your hand.",
              "list": []
            },

            {
              "title": "",
              "paragraph": "Support the infant down your arm, which is placed down (or across) your thigh.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Identify the landmark for chest compression (lower sternum approximately a finger’s breadth above the xiphisternum).",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Deliver up to 5 chest thrusts. These are similar to chest compressions, but sharper in nature and delivered at a slower rate.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Abdominal thrusts for children over 1 year:",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Stand or kneel behind the child. Place your arms under the child’s arms and encircle his torso.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Clench your fist and place it between the umbilicus and xiphisternum.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Grasp this hand with your other hand and pull sharply inwards and upwards.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Repeat up to 4 more times.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Ensure that pressure is not applied to the xiphoid process or the lower rib cage as this may cause abdominal trauma.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "The aim is to relieve the obstruction with each thrust rather than to give all 5.",
              "list": []
            },
            {
              "title": "Following chest or abdominal thrusts, reassess the child:",
              "paragraph": "If the object has not been expelled and the victim is still conscious, continue the sequence of back blows and chest (for infant) or abdominal (for children) thrusts.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Call out, or send, for help if it is still not available",
              "list": []
            },

            {
              "title": "",
              "paragraph": "Do not leave the child at this stage",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Unconscious child with choking",
              "list": []
            },
            {
              "title": "",
              "paragraph": "If the choking child is, or becomes, unconscious place him on a firm, flat surface.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Call out, or send, for help if it is still not available.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Do not leave the child at this stage.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Airway opening:",
              "list": []
            },


            {
              "title": "",
              "paragraph": "Open the mouth and look for any obvious object.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "If one is seen, make an attempt to remove it with a single finger sweep. Do not attempt blind or repeated finger sweeps – these can impact the object more deeply into the pharynx and cause injury.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Follow the BLS sequence",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Conclusion",
              "list": []
            },
            {
              "title": "",
              "paragraph": "You never know when these vital life- saving skills may be necessary. It might not even be for your own child, but that of a friend or a complete stranger who could be dependent on you having learned and practiced these skills.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Helpful hints",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Read these instructions regularly to refresh your memory:",
              "list": []
            },


            {
              "title": "",
              "paragraph": "Try to imagine an emergency before it arises so that you can anticipate what to do in that situation.",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Ensure you and your family members know how to summon emergency help and encourage them to formally learn BLS",
              "list": []
            },
            {
              "title": "",
              "paragraph": "Keep your pediatrician’s number and an ambulance number handy.",
              "list": [{
                  "id": 1,
                  "name": "REMEMBER"
                },
                {
                  "id": 2,
                  "name": "SAFETY"
                },
                {
                  "id": 3,
                  "name": "SHOUT"
                },
                {
                  "id": 4,
                  "name": "STIMULATE"
                },
                {
                  "id": 5,
                  "name": "AIRWAY"
                },
                {
                  "id": 6,
                  "name": "BREATHING"
                },
                {
                  "id": 7,
                  "name": "CIRCULATION"
                },
              ]
            },

          ]
        }, ],
      },
    }
  ]
})

export const getters = {
  services(state) {
    return state.services
  }
}
