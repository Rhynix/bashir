export const state = () => ({
    bashir: [
        {
            "para": "With Kenya MEDICAL practitioners Board. He is also a senior lecturer at the University of Nairobi in the Department of Paediatrics and Part time lecturer at The Aga Khan University Hospital Nairobi."
        },
        {
            "para": "He has admission rights in Aga Khan University, Nairobi, The Nairobi Hospital, Gertrudes children’s Hospital and M P Shah Hospital"
        },
        {
            "para": "Dr. Bashir Admani is well published in many peer reviewed journals and has written chapters in two dfferent books on paediatric nephrology and is involved in multiple research projects in Kenya."
        },
        {
            "para": "He is passionate about paediatrics and has participated in advocacy in many for a like newspaper articles, national television interviews and school talks. He has been the secretary of the African Pediatric Nephrology Association since 2010 and a member of Kenya Paediatric Association and Kenya Renal Association."
        },
        {
            "para": "Dr. Bashir Admani does his outpatient work from The Aga Khan University Hospital Doctors Plaza and liases with our branches in Karen and Lavington. He would be overlooking the opening of the Pediatric Renal unit at Doctors Park very soon."
        }
    ],
    anne: [
        {
            "para": "Having worked in various institutions including the Aga Khan University Hospital and AAR."
        },
        {
            "para": "She completed her paediatric specialist training at the University of Nairobi and is pursuing a fellowship in Infectious Disease at the University of Nairobi in collaboration with the University of Maryland, Baltimore.  She has special interest in vaccinology and travel medicine."
        },
        {
            "para": "She is a member of the Kenya Paediatric Association and the European Society for Paediatric Infectious Diseases."
        },
    ],
    farida: [
        {
            "para": "She is a paediatrician passionate about all things child health. She is the National Secretary of Kenya Paediatric Association. She is trained in EPLS and paediatrics in disasters. In addition to her medical training, she is a mother of 4 and loves dealing with kids. "
        },
    ],
})

export const getters = {
    bashir (state) {
        return state.bashir
    },
    anne (state) {
        return state.anne
    },
    farida (state) {
        return state.farida
    }
}

export const mutations = {
    SET_CATEGORIES (state, categories) {
        state.categories = categories
    }
}

export const actions = {
    
}